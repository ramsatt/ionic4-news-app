import { Component, OnInit } from '@angular/core';
import {NewsService} from '../services/news.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-health',
  templateUrl: './health.page.html',
  styleUrls: ['./health.page.scss'],
})
export class HealthPage implements OnInit {
    newsArray: any = [];
    constructor(private news: NewsService, private router: Router) { }

    ngOnInit() {
        this.loadHeadLines('health');
    }

    loadHeadLines(category) {
        this.news.getNewbyCategory(category).subscribe(news => {
            this.newsArray = news['articles'];
            console.log(this.newsArray);
        });
    }

    getDetails(news) {
        this.router.navigate(['/newsdetail', { 'title': news.title, 'desc': news.description, 'img': news.urlToImage, 'url': news.url }]);
    }
}
